package com.volatileapps.a20220503_jarodsmith_nycschools;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ListviewAdapter extends ArrayAdapter<SchoolObjects> {
    public ListviewAdapter(Context context, ArrayList<SchoolObjects> schools) {
        super(context, 0, schools);
    }
    SchoolObjects schoolObjects;
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        schoolObjects = getItem(position);

        if (convertView ==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.customcell,parent,false);
        }
        TextView schoolName = convertView.findViewById(R.id.schoolName);
        TextView schoolName2 = convertView.findViewById(R.id.schoolName2);
        schoolName.setText(schoolObjects.getSchool_name());
        schoolName2.setText(schoolObjects.getCity()+", "+schoolObjects.getState_code());

        return convertView;
    }
}
