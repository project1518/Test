package com.volatileapps.a20220503_jarodsmith_nycschools;

public class SchoolObjects {
    private String dbn;
    private String school_name;
    private String overview_paragraph;
    private String city;
    private String state_code;

    public SchoolObjects(String dbn, String school_name, String overview_paragraph, String city, String state_code) {
        this.dbn = dbn;
        this.school_name = school_name;
        this.overview_paragraph = overview_paragraph;
        this.city = city;
        this.state_code = state_code;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph(String overview_paragraph) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }
}
