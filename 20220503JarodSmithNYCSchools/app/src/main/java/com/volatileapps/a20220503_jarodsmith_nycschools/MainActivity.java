package com.volatileapps.a20220503_jarodsmith_nycschools;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Currency;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private String TAG = "TESTER";

    private String schoolJSON ="";
    private String schoolDetailJSON="";

    private String urlSchoolDetail = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";
    private String urlSchool = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    private ArrayList<SchoolObjects> schoolObjects= new ArrayList<>();
    private ArrayList<SchoolSatObjects> schoolSatObjects= new ArrayList<>();

    ArrayAdapter<SchoolObjects> schoolArrayAdapter;

    private ListView listView;
    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = new ListView(this);
        listView = findViewById(R.id.mainListview);

        //get JSON
        AsyncTaskGetJSON task = new AsyncTaskGetJSON();
        task.execute();

    }


    public class AsyncTaskGetJSON extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected String doInBackground(String... strings) {
            schoolJSON = getJSON(urlSchool);
            schoolDetailJSON = getJSON(urlSchoolDetail);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(MainActivity.this,"LOADING...", Toast.LENGTH_SHORT).show();
            //parse json
            try {
                ParseSchoolJSON();
                ParseSchoolDetailJSON();
            } catch (JSONException e) {
                e.printStackTrace();
                //Log.d(TAG, "onPostExecute: CATCH "+e);
            }
            //load the listview
            schoolArrayAdapter = new ListviewAdapter(MainActivity.this,schoolObjects);
            listView.setAdapter(schoolArrayAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.d(TAG, "onItemClick: "+i);
                    String schoolName = schoolObjects.get(i).getSchool_name();
                    String dbn = schoolObjects.get(i).getDbn();
                    String overview = schoolObjects.get(i).getOverview_paragraph();
                    String math="";
                    String reading="";
                    String writing="";
                    for (int j =0;j<schoolSatObjects.size();j++){
                        if (schoolSatObjects.get(j).dbn.contains(dbn)){
                            math = schoolSatObjects.get(j).getSat_math_avg_score();
                            reading = schoolSatObjects.get(j).getSat_critical_reading_avg_score();
                            writing = schoolSatObjects.get(j).getSat_writing_avg_score();
                        }
                    }

                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra ("name",schoolName);
                    intent.putExtra("overview", overview);
                    intent.putExtra("dbn", dbn);
                    intent.putExtra("math",math);
                    intent.putExtra("reading",reading);
                    intent.putExtra("writing",writing);

                    startActivity(intent);
                }
            });
        }

    }
    public String getJSON(String url){
        String jsonText="";

        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = client.newCall(request);
            Response response = call.execute();
            jsonText = response.body().string();
            response.body().close();
            call.cancel();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "doInBackground: "+e);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Connectivity issue, check your network connection and reload",Toast.LENGTH_SHORT).show();
                }
            });
        }
        return jsonText;
    }

    public void ParseSchoolJSON() throws JSONException {
        JSONArray jsonArray = new JSONArray(schoolJSON);
        for (int i =0; i<jsonArray.length();i++){
            JSONObject object = jsonArray.getJSONObject(i);

            String dbn= object.getString("dbn");
            String school_name= object.getString("school_name");
            String overview_paragraph= object.getString("overview_paragraph");
            String city= object.getString("city");
            String state_code= object.getString("state_code");
            schoolObjects.add(new SchoolObjects(
                    dbn,
                    school_name,
                    overview_paragraph,
                    city,
                    state_code));
        }
    }
    public void ParseSchoolDetailJSON() throws JSONException {
        JSONArray jsonArray = new JSONArray(schoolDetailJSON);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            String dbn = object.getString("dbn");
            String school_name = object.getString("school_name");
            String num_of_sat_test_takers = object.getString("num_of_sat_test_takers");
            String sat_critical_reading_avg_score = object.getString("sat_critical_reading_avg_score");
            String sat_math_avg_score = object.getString("sat_math_avg_score");
            String sat_writing_avg_score = object.getString("sat_writing_avg_score");
            schoolSatObjects.add(new SchoolSatObjects(
                    dbn,
                    school_name,
                    num_of_sat_test_takers,
                    sat_critical_reading_avg_score,
                    sat_math_avg_score,
                    sat_writing_avg_score));
        }
    }



}