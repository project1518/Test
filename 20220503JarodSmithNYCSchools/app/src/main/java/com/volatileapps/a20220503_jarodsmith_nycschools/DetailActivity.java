package com.volatileapps.a20220503_jarodsmith_nycschools;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schooldetails);
        TextView tvName = findViewById(R.id.schoolSatName);
        TextView tvOverview = findViewById(R.id.schoolOverview);
        TextView tvMath = findViewById(R.id.satMath);
        TextView tvReading = findViewById(R.id.satReading);
        TextView tvWriting = findViewById(R.id.satWriting);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("key");
            String name = extras.getString("name");
            String overview = extras.getString("overview");
            String math = extras.getString("math");
            String reading = extras.getString("reading");
            String writing = extras.getString("writing");
            tvName.setText(name);
            tvOverview.setText(overview);
            tvMath.setText("Math average: "+math);
            tvReading.setText("Reading average: "+reading);
            tvWriting.setText("Writing average: "+writing);
        }

    }
}
